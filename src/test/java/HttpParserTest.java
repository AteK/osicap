

import java.io.*;

public class HttpParserTest {

    private static String[] httpMethods = {"GET", "HEAD", "POST", "PUT", "DELETE", "CONNECT", "OPTIONS", "TRACE", "PATCH"};

    public static void main(String[] args) {

        File file = new File("text.http");

        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line;
            StringBuilder builder = new StringBuilder();

            while ((line = reader.readLine()) != null)
                builder.append(line).append("\n");

            reader.close();

            String httpText = builder.toString();

            for (String m : httpMethods) {

                if (httpText.contains(m)) {
                    String plainText = m + httpText.split(m)[1];
                    System.out.println(plainText);

                    HttpParser parser = new HttpParser(plainText);

                    System.out.println(parser.getParam("app_id"));
                    break;
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
