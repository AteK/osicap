package osicap.ui;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class OsicapUi extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(this.getClass().getResource("/UI.fxml"));

        stage.setTitle("Osicap");

        Scene scene = new Scene(root);
        scene.getStylesheets().add("/style.css");

        stage.setScene(scene);
        stage.setResizable(false);
        stage.setWidth(1000);
        stage.setHeight(610);
        stage.setOnCloseRequest(event -> System.exit(0));
        stage.show();
    }

    public static void start() {
        launch(OsicapUi.class);
    }
}
