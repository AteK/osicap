package osicap.ui;

import com.jfoenix.controls.JFXComboBox;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import javafx.scene.text.Text;
import org.pcap4j.core.PcapNativeException;
import org.pcap4j.core.Pcaps;
import osicap.Osicap;
import osicap.util.LocalAddress;
import osicap.util.MacFinder;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;



public class HomeController implements Initializable {

    @FXML
    private Text interfaceDisplayName;

    @FXML
    private JFXComboBox<String> interfaceBox;

    @FXML
    private Text interfaceName;

    @FXML
    private Text ipName;

    @FXML
    private Text macName;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        try {

            List<NetworkInterface> networkInterfaces = Collections.list(NetworkInterface.getNetworkInterfaces());

            for (NetworkInterface i : networkInterfaces)
                this.interfaceBox.getItems().add(i.getName());

            this.interfaceBox.setCellFactory(param -> {
                ListCell<String> cell = new ListCell<String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(empty ? null : item);
                    }
                };

                cell.setOnMouseEntered(e -> {
                    try {
                        ipName.setText("");
                        NetworkInterface networkInterface = NetworkInterface.getByName(cell.getItem());

                        interfaceName.setText(networkInterface.getName());
                        interfaceDisplayName.setText(networkInterface.getDisplayName());

                        List<InetAddress> addresses = Collections.list(networkInterface.getInetAddresses());

                        for (InetAddress address : addresses)
                            ipName.setText(ipName.getText() + "   " + address.getHostAddress());

                        if (networkInterface.getHardwareAddress() != null)
                            macName.setText(MacFinder.decalculate_mac(networkInterface.getHardwareAddress()));

                    } catch (SocketException e1) {
                        e1.printStackTrace();
                    }
                });

                return cell;
            });
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }


    @FXML
    private void setInterface() {
        try {
             NetworkInterface nic = NetworkInterface.getByName(interfaceBox.getValue());

             Osicap.localAddress = new LocalAddress(nic);
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }
}
