package osicap.ui;

import com.jfoenix.controls.JFXTabPane;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Tab;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by atek on 14/08/17.
 */
public class UIController implements Initializable {

    @FXML
    private JFXTabPane tabpane;

    @FXML
    private AnchorPane root;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        try {
            Tab home = new Tab("Home");

            Parent homeUi = FXMLLoader.load(this.getClass().getResource("/Home.fxml"));
            home.setContent(homeUi);

            Tab networkDiscoveryTab = new Tab("Network Discovery");

            Parent networkDiscoveryUi = FXMLLoader.load(this.getClass().getResource("/NetworkDiscovery.fxml"));
            networkDiscoveryTab.setContent(networkDiscoveryUi);

            Tab mitm = new Tab("Man In The Middle");

            Parent mitmUi = FXMLLoader.load(this.getClass().getResource("/Mitm.fxml"));
            mitm.setContent(mitmUi);

            tabpane.getTabs().addAll(home, networkDiscoveryTab, mitm);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
