package osicap.ui;

import com.jfoenix.controls.JFXProgressBar;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;
import org.pcap4j.util.MacAddress;
import osicap.Osicap;
import osicap.util.MacFinder;
import osicap.util.NetworkDiscovery;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.util.ResourceBundle;


public class NetworkDiscoveryController implements Initializable {

    @FXML
    private JFXTreeTableView tableView;

    @FXML
    private JFXProgressBar progressBar;

    @FXML
    private Text statusText;

    private final ListViewData root = new ListViewData("", "", "");

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        JFXTreeTableColumn<ListViewData, String> name = new JFXTreeTableColumn<>("Name");
        name.setCellValueFactory(param -> param.getValue().getValue().nameProperty());

        JFXTreeTableColumn<ListViewData, String> ipAddress = new JFXTreeTableColumn<>("Ip");
        ipAddress.setCellValueFactory(param -> param.getValue().getValue().ipAddressProperty());

        JFXTreeTableColumn<ListViewData, String> macAddress = new JFXTreeTableColumn<>("Mac");
        macAddress.setCellValueFactory(param -> param.getValue().getValue().macAddressProperty());

        tableView.getColumns().addAll(name, ipAddress, macAddress);

        RecursiveTreeItem<ListViewData> rootItem = new RecursiveTreeItem<>(root, ListViewData::getDatas);

        tableView.setRoot(rootItem);

    }

    @FXML
    private void discover() {
        NetworkDiscovery.Status status = new NetworkDiscovery.Status() {
            @Override
            public void onHostRequested(InetAddress hostTested, int percent) {
                Platform.runLater(() -> {
                    double p = ((double) percent) / 100;
                    progressBar.setProgress(p);
                    statusText.setText("Ping request sent to " + hostTested.getHostAddress());
                });
            }

            @Override
            public void onHostInterrogating(InetAddress hostTested) {
                Platform.runLater(() -> statusText.setText("Finding mac of " + hostTested.getHostAddress()));
            }

            @Override
            public void onMacFound(InetAddress hostTested, MacAddress address) {
                Platform.runLater(() -> root.add(new ListViewData(hostTested.getHostName(), hostTested.getHostAddress(), MacFinder.decalculate_mac(address.getAddress()))));

            }

            @Override
            public void onMacNotFound(InetAddress hostTested) {
                Platform.runLater(() -> {
                    statusText.setText("cannot find mac of " + hostTested.getHostAddress());
                    root.add(new ListViewData(hostTested.getHostName(), hostTested.getHostAddress(), "Mac not found"));
                });
            }
        };

        DiscoveryTask discoveryTask = new DiscoveryTask(status);

        Thread thread = new Thread(discoveryTask);
        thread.start();

    }

    private static class ListViewData extends RecursiveTreeObject<ListViewData> {

        private StringProperty name, ipAddress, macAddress;

        private final ObservableList<ListViewData> list = FXCollections.observableArrayList();


        public ListViewData(String name, String ipAddress, String macAddress) {
            this.name = new SimpleStringProperty(name);
            this.ipAddress = new SimpleStringProperty(ipAddress);
            this.macAddress = new SimpleStringProperty(macAddress);
        }

        public void add(ListViewData data) {
            this.list.add(data);
        }
        public void addAll(ListViewData... data) {
            this.list.addAll(data);
        }

        public String getName() {
            return name.get();
        }

        public StringProperty nameProperty() {
            return name;
        }

        public String getIpAddress() {
            return ipAddress.get();
        }

        public StringProperty ipAddressProperty() {
            return ipAddress;
        }

        public String getMacAddress() {
            return macAddress.get();
        }

        public StringProperty macAddressProperty() {
            return macAddress;
        }

        public ObservableList<ListViewData> getDatas() {
            return list;
        }

    }

    private class DiscoveryTask implements Runnable {

        private NetworkDiscovery.Status status;

        public DiscoveryTask(NetworkDiscovery.Status status) {
            this.status = status;
        }

        @Override
        public void run() {
            NetworkDiscovery discovery = new NetworkDiscovery();
            discovery.findHosts(Osicap.localAddress.getNetworkInterface(), status);
        }
    }



}
