package osicap.arp;


import org.pcap4j.core.NotOpenException;
import org.pcap4j.core.PcapHandle;
import org.pcap4j.core.PcapNativeException;
import org.pcap4j.packet.ArpPacket;
import org.pcap4j.packet.EthernetPacket;
import org.pcap4j.packet.namednumber.ArpHardwareType;
import org.pcap4j.packet.namednumber.ArpOperation;
import org.pcap4j.packet.namednumber.EtherType;
import org.pcap4j.util.ByteArrays;
import org.pcap4j.util.MacAddress;

import java.net.Inet4Address;

public class ArpSender {

    private PcapHandle handle;
    private MacAddress srcMacAddress;
    private Inet4Address srcIpV4Address;

    /**
     * https://en.wikipedia.org/wiki/Address_Resolution_Protocol
     */

    public ArpSender(PcapHandle handle, MacAddress srcMacAddress, Inet4Address srcIpV4Address) {
        this.handle = handle;
        this.srcMacAddress = srcMacAddress;
        this.srcIpV4Address = srcIpV4Address;
    }

    public void send(Inet4Address dstAddress, ArpOperation operation) throws PcapNativeException, NotOpenException {
        ArpPacket.Builder arpBuilder = new ArpPacket.Builder();

        // Contruction d'une requête arp basique.

        arpBuilder.dstHardwareAddr(MacAddress.ETHER_BROADCAST_ADDRESS)
                .srcHardwareAddr(srcMacAddress)
                .dstProtocolAddr(dstAddress)
                .srcProtocolAddr(this.srcIpV4Address)
                .hardwareType(ArpHardwareType.ETHERNET)
                .protocolType(EtherType.IPV4)
                .hardwareAddrLength((byte) MacAddress.SIZE_IN_BYTES)
                .protocolAddrLength((byte) ByteArrays.INET4_ADDRESS_SIZE_IN_BYTES)
                .operation(operation);


        this.send(arpBuilder, MacAddress.ETHER_BROADCAST_ADDRESS);

    }

    public void send(Inet4Address dstIpV4Address, MacAddress dstMacAddress, ArpOperation operation) throws PcapNativeException, NotOpenException {
        ArpPacket.Builder arpBuilder = new ArpPacket.Builder();

        arpBuilder
                .dstHardwareAddr(dstMacAddress)
                .srcHardwareAddr(srcMacAddress)
                .dstProtocolAddr(dstIpV4Address)
                .srcProtocolAddr(this.srcIpV4Address)
                .hardwareType(ArpHardwareType.ETHERNET)
                .protocolType(EtherType.IPV4)
                .hardwareAddrLength((byte) MacAddress.SIZE_IN_BYTES)
                .protocolAddrLength((byte) ByteArrays.INET4_ADDRESS_SIZE_IN_BYTES)
                .operation(operation);


        this.send(arpBuilder, dstMacAddress);
    }

    public void send(ArpPacket.Builder arpBuilder, MacAddress dst) throws PcapNativeException, NotOpenException {

        // Construction du paquet ethernet (Couche la plus basse dans un paquet https://fr.wikipedia.org/wiki/Mod%C3%A8le_OSI)
        EthernetPacket.Builder ethernetBuilder = new EthernetPacket.Builder();
        ethernetBuilder.srcAddr(srcMacAddress)
                .dstAddr(dst)
                .type(EtherType.ARP)
                .payloadBuilder(arpBuilder)
                .paddingAtBuild(true);

        EthernetPacket ethernetPacket = ethernetBuilder.build();

        handle.sendPacket(ethernetPacket);
    }

    public PcapHandle getHandle() {
        return handle;
    }

    public void close() {
        handle.close();
    }
}
