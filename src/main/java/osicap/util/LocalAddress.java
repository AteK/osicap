package osicap.util;


import org.pcap4j.util.MacAddress;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class LocalAddress {

    private NetworkInterface networkInterface;

    public LocalAddress(NetworkInterface networkInterface) {
        this.networkInterface = networkInterface;
    }

    public MacAddress getHardwareAddress()  {
        byte[] mac = new byte[0];
        try {
            mac = this.networkInterface.getHardwareAddress();
        } catch (SocketException e) {
            e.printStackTrace();
        }

        return MacAddress.getByAddress(mac);
    }

    public Inet4Address getInet4Address() {
        InetAddress ipv4_adress = null;

        Enumeration<InetAddress> en = this.networkInterface.getInetAddresses();

        while (en.hasMoreElements()) {
            InetAddress a = en.nextElement();

            if (a.getAddress().length == 4)
                ipv4_adress = a;
        }

        return (Inet4Address) ipv4_adress;
    }

    public NetworkInterface getNetworkInterface() {
        return networkInterface;
    }
}
