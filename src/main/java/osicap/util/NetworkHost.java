package osicap.util;


import org.pcap4j.util.MacAddress;

import java.net.Inet4Address;

public class NetworkHost {

    private Inet4Address ip_address;
    private MacAddress mac_address;

    public NetworkHost(Inet4Address ip_address, MacAddress mac_address) {
        this.ip_address = ip_address;
        this.mac_address = mac_address;
    }

    public Inet4Address getIpAddress() {
        return ip_address;
    }

    public MacAddress getMacAddress() {
        return mac_address;
    }
}
