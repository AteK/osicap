package osicap.util;


import org.pcap4j.core.*;
import org.pcap4j.packet.ArpPacket;
import org.pcap4j.packet.namednumber.ArpOperation;
import org.pcap4j.util.MacAddress;
import osicap.packet.ArpSender;
import osicap.packet.PacketSender;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.NetworkInterface;
import java.net.SocketException;

public class MacFinder {

    private static MacAddress resolveAddress;
    private Inet4Address address;
    private NetworkInterface networkInterface;
    private PcapHandle handle;
    private PacketSender sender;

    private Task task;


    public MacFinder(NetworkInterface networkInterface, Inet4Address address) throws PcapNativeException {
        this.address = address;
        this.networkInterface = networkInterface;
        LocalAddress localAddress = new LocalAddress(networkInterface);
        PcapNetworkInterface pcapNetworkInterface = Pcaps.getDevByAddress(localAddress.getInet4Address());
        handle = pcapNetworkInterface.openLive(65536, PcapNetworkInterface.PromiscuousMode.PROMISCUOUS, 10);

        /**
         * On crée un listener pour attendre la reponse de l'hote
         */

        PacketListener listener = packet -> {

            if (packet.contains(ArpPacket.class)) {
                ArpPacket arpPacket = packet.get(ArpPacket.class);
                // On verifie que l'operation packet est bien égal à reply.
                if (arpPacket.getHeader().getOperation() == ArpOperation.REPLY && arpPacket.getHeader().getSrcProtocolAddr().getHostAddress().equals(address.getHostAddress())) {
                    resolveAddress = arpPacket.getHeader().getSrcHardwareAddr();
                }
            }

        };

        // On crée un nouvelle Thread pour le listener
        this.task = new Task(listener, handle);

        Thread thread = new Thread(task);
        thread.start();
    }

    public MacFinder(PcapNetworkInterface networkInterface, Inet4Address address) throws PcapNativeException, SocketException {
        this.address = address;
        this.handle = networkInterface.openLive(65536, PcapNetworkInterface.PromiscuousMode.PROMISCUOUS, 10);
        this.networkInterface = NetworkInterface.getByName(networkInterface.getName());

        /**
         * On crée un listener pour attendre la reponse de l'hote
         */

        PacketListener listener = packet -> {

            if (packet.contains(ArpPacket.class)) {
                ArpPacket arpPacket = packet.get(ArpPacket.class);
                // On verifie que l'operation packet est bien égal à reply.
                if (arpPacket.getHeader().getOperation() == ArpOperation.REPLY && arpPacket.getHeader().getSrcProtocolAddr().getHostAddress().equals(address.getHostAddress())) {

                    resolveAddress = arpPacket.getHeader().getSrcHardwareAddr();
                }
            }

        };

        // On crée un nouvelle Thread pour le listener
        this.task = new Task(listener, handle);

        Thread thread = new Thread(task);
        thread.start();
    }

    /**
     *
     * @param count
     * @return
     * @throws IOException
     * @throws PcapNativeException
     * @throws NotOpenException
     *
     * Count est le temps que va attendre le listener pour avoir la réponse de l'hote.
     * .
     */

    public MacAddress find( int count) throws IOException, PcapNativeException, NotOpenException {

        resolveAddress = MacAddress.ETHER_BROADCAST_ADDRESS;

        LocalAddress addr = new LocalAddress(networkInterface);

        PacketSender packetSender = new PacketSender(this.handle);

        ArpSender sender = new ArpSender(packetSender, addr.getHardwareAddress(), addr.getInet4Address());

        sender.sendArp(address, ArpOperation.REQUEST);

        for (int i = 0; i < count; i++) {


            if (!(resolveAddress == MacAddress.ETHER_BROADCAST_ADDRESS)) {
                return resolveAddress;
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }



        return null;
    }

    /**
     *
     * @param mac
     * @return String
     *
     * Permet de transformer une adresse mac en byte en adresse mac lisible sous forme 00:00:00:00:00:00
     */

    public static String decalculate_mac(byte[] mac){
        StringBuilder sb = new StringBuilder(18);
        for (byte b : mac) {
            if (sb.length() > 0)
                sb.append(':');
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

    public void close() {
        this.handle.close();
        this.task.close();
    }

    // On lance la Thread avec le listener.
    private static class Task implements Runnable {

        private PacketListener listener;
        private PcapHandle handle;

        public Task(PacketListener listener, PcapHandle handle) {
            this.listener = listener;
            this.handle = handle;
        }

        @Override
        public void run() {
            try {
                this.handle.loop(10, listener); // On lance le listener
            } catch (PcapNativeException | NotOpenException | InterruptedException e) {
                e.printStackTrace();
            }
        }
        public void close() {
            this.handle.close();
        }
    }


}
