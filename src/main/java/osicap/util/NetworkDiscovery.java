package osicap.util;


import org.pcap4j.core.NotOpenException;
import org.pcap4j.core.PcapNativeException;
import org.pcap4j.util.MacAddress;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;

import static java.lang.System.out;

public class NetworkDiscovery {


    // On ping tous les ip possible.
    public NetworkHost[] findHosts(NetworkInterface inter, Status status) {
        Inet4Address ipv4_adress = new LocalAddress(inter).getInet4Address();

        if (ipv4_adress.getAddress() == null) {
            out.printf("Not a valid network interface, must contains ipv4 interface");
            System.exit(-1);
        }


        ArrayList<InetAddress> ipFound = new ArrayList<>();

        byte[] ip = ipv4_adress.getAddress();

        for (int i = 0; i < 255; i++) {

            double percent = ((double) i / 255) * 100;

            try {

                ip[3] = (byte) i;

                status.onHostRequested(InetAddress.getByAddress(ip), (int) percent);

                if (ip == ipv4_adress.getAddress())
                    continue;

                if (InetAddress.getByAddress(ip).isReachable(2000)) // On ping l'hote avec un delai max de 2 sec de reponse
                    ipFound.add(InetAddress.getByAddress(ip));

            } catch (IOException e) {
                e.printStackTrace();
            }
        }



        ArrayList<NetworkHost> hosts = new ArrayList<>();

        try {

            Thread.sleep(2000);

            for (InetAddress addr : ipFound) {
                MacFinder finder = new MacFinder(inter, (Inet4Address) addr);

                status.onHostInterrogating(addr);

                MacAddress macAddress = finder.find(3);


                if (macAddress != null) {
                    status.onMacFound( addr, macAddress );
                    hosts.add( new NetworkHost( (Inet4Address) addr, macAddress) );
                    finder.close();
                    continue;
                }
                finder.close();

                status.onMacNotFound(addr);
                hosts.add( new NetworkHost( (Inet4Address) addr, null));
            }

        } catch (PcapNativeException | IOException | NotOpenException | InterruptedException e) {
            e.printStackTrace();
        }

        NetworkHost[] h = new NetworkHost[hosts.size()];
        h = hosts.toArray(h);

        return h;

    }

    public interface Status {

        void onHostRequested(InetAddress hostTested, int percent);
        void onHostInterrogating(InetAddress hostTested);
        void onMacFound(InetAddress hostTested, MacAddress address);
        void onMacNotFound(InetAddress hostTested);
    }


}
