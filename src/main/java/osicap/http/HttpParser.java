package osicap.http;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by root on 10/10/17.
 */
public class HttpParser {

    private static String[] httpMethods = {"GET", "HEAD", "POST", "PUT", "DELETE", "CONNECT", "OPTIONS", "TRACE", "PATCH"};

    private String httpText;

    private String method;
    private String url;
    private String httpVersion;

    private HashMap<String, String> headers = new HashMap<>();
    private HashMap<String, String> params = new HashMap<>();

    public HttpParser(String httpText) {
        this.httpText = httpText;

        String[] lines = this.httpText.split("\\r?\\n");

        String[] cmd = lines[0].split(" ");

        if (Arrays.asList(httpMethods).contains(cmd[0])) {
            this.method = cmd[0];
            this.url = cmd[1];
            this.httpVersion = cmd[2];

            for (int i = 0; i < lines.length; i++) {
                String s = lines[i];

                // on check si il y a des parametre a requete
                if (s.equals("") && i + 1 <= lines.length) {
                    String p = lines[i + 1];

                    String[] params = p.split("&");

                    if (params.length > 0) {

                        for (String str : params)
                            this.params.put(str.split("=")[0], str.split("=")[1]);
                    }

                } else if (s.equals("")) {
                    break;
                }

                int index = s.indexOf(":");

                if (index < 0 )
                    continue;

                String key = s.substring(0, index);
                String para = s.substring(index+1).trim();

                headers.put(key, para);
            }
        }

    }

    public String getHeader(String key) {

        if (this.headers.containsKey(key))
            return this.headers.get(key);

        return "null";
    }

    public String getHeaders() {

        StringBuilder builder = new StringBuilder();

        for (String key : this.headers.keySet())
            builder.append(key).append("=").append(this.headers.get(key)).append("\n");

        return builder.toString();
    }

    public String getParam(String key) {

        if (this.params.containsKey(key))
            return this.params.get(key);

        return "null";
    }

    public String getParams() {
        StringBuilder builder = new StringBuilder();

        for (String key : this.params.keySet())
            builder.append(key).append("=").append(this.params.get(key)).append("\n");

        return builder.toString();
    }

    public String getMethod() {
        return method;
    }

    public String getUrl() {
        return url;
    }

    public String getHttpVersion() {
        return httpVersion;
    }
}
