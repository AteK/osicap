package osicap.mitm;

import org.pcap4j.core.NotOpenException;
import org.pcap4j.core.PacketListener;
import org.pcap4j.core.PcapHandle;
import org.pcap4j.core.PcapNativeException;
import org.pcap4j.packet.ArpPacket;
import org.pcap4j.packet.EthernetPacket;
import org.pcap4j.packet.IpV4Packet;
import org.pcap4j.packet.Packet;
import osicap.packet.PacketSender;
import osicap.util.LocalAddress;
import osicap.util.NetworkHost;

import javax.swing.event.EventListenerList;
import java.net.SocketException;


public class MiddleHandler implements PacketListener, Runnable {

    private NetworkHost incoming;
    private NetworkHost outgoing;
    private LocalAddress localAddress;

    private PcapHandle handle;
    private PacketSender sender;

    private boolean isBlocked = false;

    private final EventListenerList listeners = new EventListenerList();

    public MiddleHandler(PcapHandle handle, NetworkHost incoming, NetworkHost outgoing, LocalAddress localAddress) {
        this.handle = handle;
        this.incoming = incoming;
        this.outgoing = outgoing;
        this.localAddress = localAddress;
        this.sender = new PacketSender(handle);
    }

    @Override
    public void run() {
        try {
            this.handle.loop(-1, this);
        } catch (PcapNativeException | InterruptedException | NotOpenException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotPacket(Packet packet) {

        // On verife que le paquet contienne un paquet IpV4 et que ce ne soit pas un paquet de type ARP.
        if (!packet.contains(IpV4Packet.class) || packet.contains(ArpPacket.class) || isBlocked)
            return;


        try {

            IpV4Packet ipV4Packet = packet.get(IpV4Packet.class);

            String srcAddress = ipV4Packet.getHeader().getSrcAddr().getHostAddress();
            String dstAddress = ipV4Packet.getHeader().getDstAddr().getHostAddress();

            // On redirige le paquet ui provient de la victime au router
            if (srcAddress.equals(incoming.getIpAddress().getHostAddress())) {
                Packet spoof = spoofPacket(packet, outgoing);
                System.out.println(spoof);
                this.sender.sendPacket(spoof);

                onFireEvent(packet);
            }

            // On redirige le paquet qui provient du router à la victim.
            if (dstAddress.equals(incoming.getIpAddress().getHostAddress())) {
                Packet spoof = spoofPacket(packet, incoming);
                this.sender.sendPacket(spoof);

                onFireEvent(packet);
            }

        } catch (PcapNativeException | NotOpenException | SocketException e) {
            e.printStackTrace();
        }
    }

    // On change remplace seulement l'address mac source par la notre
    private Packet spoofPacket(Packet packet, NetworkHost dst) throws SocketException {
        EthernetPacket ethernetPacket = packet.get(EthernetPacket.class);

        EthernetPacket.Builder spoofEthernetPacket = new EthernetPacket.Builder()
                .srcAddr(this.localAddress.getHardwareAddress())
                .dstAddr(dst.getMacAddress())
                .type(ethernetPacket.getHeader().getType())
                .payloadBuilder(ethernetPacket.getPayload().getBuilder())
                .pad(ethernetPacket.getPad())
                .paddingAtBuild(true);

        return spoofEthernetPacket.build();
    }

    public void block(boolean bool) {
        this.isBlocked = bool;
    }

    // Permet d'appeler les differents listeners.
    public void onFireEvent(Packet packet) {

        for (MiddleHandlerListener listener : this.listeners.getListeners(MiddleHandlerListener.class))
            listener.onPacketCaptured(packet);
    }

    public void addListener(MiddleHandlerListener... middleHandlerListener) {
        for (MiddleHandlerListener listener : middleHandlerListener)
            this.listeners.add(MiddleHandlerListener.class, listener);
    }

    public void removeListener(MiddleHandlerListener... middleHandlerListeners) {
        for (MiddleHandlerListener listener : middleHandlerListeners)
            this.listeners.remove(MiddleHandlerListener.class, listener);
    }

    // Ferme le handle
    public void doStop() {
        this.handle.close();
        this.sender.close();
        System.out.println("Middle Handler Closed");
    }

}
