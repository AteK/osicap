package osicap.mitm;

import org.pcap4j.packet.Packet;

import java.util.EventListener;


public interface MiddleHandlerListener extends EventListener {

    void onPacketCaptured(Packet packet);
}
