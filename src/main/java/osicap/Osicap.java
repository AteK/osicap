package osicap;


import org.pcap4j.core.PcapNetworkInterface;
import osicap.ui.OsicapUi;
import osicap.util.LocalAddress;

import java.net.NetworkInterface;

public class Osicap {


    public static LocalAddress localAddress;

    public static void main(String[] args) {
        OsicapUi.start();
    }
}
