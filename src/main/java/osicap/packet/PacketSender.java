package osicap.packet;


import org.pcap4j.core.NotOpenException;
import org.pcap4j.core.PcapHandle;
import org.pcap4j.core.PcapNativeException;
import org.pcap4j.packet.EthernetPacket;
import org.pcap4j.packet.Packet;
import org.pcap4j.packet.namednumber.EtherType;
import org.pcap4j.util.MacAddress;

public class PacketSender {

    private PcapHandle handle;

    public PacketSender(PcapHandle handle) {
        this.handle = handle;
    }

    /**
     *
     * @param builder
     * @param type
     * @param srcMac
     * @param dstMac
     * @throws PcapNativeException
     * @throws NotOpenException
     *
     * Construction d'un paquet Ethernet qui est indispensable dans la communication dans un réseau local
     */

    public void sendPacket(Packet.Builder builder, EtherType type, MacAddress srcMac, MacAddress dstMac) throws PcapNativeException, NotOpenException {
        EthernetPacket.Builder ethernetBuilder = new EthernetPacket.Builder();
        ethernetBuilder.srcAddr(srcMac)
                .dstAddr(dstMac)
                .type(type)
                .payloadBuilder(builder)
                .paddingAtBuild(true);

        EthernetPacket ethernetPacket = ethernetBuilder.build();
        this.sendPacket(ethernetPacket);
    }

    /**
     *
     * @param packet qui va ensuite être envoyé.
     * @throws PcapNativeException
     * @throws NotOpenException
     */
    public void sendPacket(Packet packet) throws PcapNativeException, NotOpenException {
        handle.sendPacket(packet);
    }

    public PcapHandle getHandle() {
        return handle;
    }

    public void close() {
        handle.close();
    }
}
